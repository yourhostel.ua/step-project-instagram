import express from 'express';
import morgan from "morgan";
import methodOverride from "method-override";
import cors from "cors";
import usersRouter from "../routes/users-routes.js";
import apiUsersRouter from "../routes/api-users-routes.js";
import postsRouter from "../routes/posts-routes.js";
import apiPostsRouter from "../routes/api-posts-routes.js";
import commentsRouter from "../routes/comments-routes.js";
import apiCommentsRouter from "../routes/api-comments-routes.js";
import secureRouter from "../routes/users-secure-routes.js";
import path from "path";
import createPath, {__dirname} from "../helpers/create-path.js";
import authMiddleware from "./authMiddleware.js";

export default function createServer() {
    const app = express()
    //     //для логування HTTP-запитів
    app.use(morgan(':method :url :status :res[content-length] - :response-time ms'))
    //     //для парсинга вхідного запиту
    app.use(express.urlencoded({extended: false}))
    //     //для відстеження прапора put
    app.use(methodOverride('_method'))
    app.use(express.json())
    app.use(cors());
    app.use('/auth', secureRouter);
    app.use(authMiddleware)//авторизація - якщо ні, нижче все заблоковано
    app.use(usersRouter);
    app.use(apiUsersRouter);
    app.use(postsRouter);
    app.use(apiPostsRouter);
    app.use(commentsRouter);
    app.use(apiCommentsRouter);

    //повертаємо реакт додаток

    app.use(express.static(path.join(__dirname, '..', '../build')));

    app.get('*', (req, res) => {
        res.sendFile(path.join(__dirname, '..', '../build/index.html'));
    });

    app.use((req, res) => {
        res
            .status(404)
            .render(createPath('answer'))
    })

    return app;
}

