import express from 'express';
const router = express.Router();
import auth from "../controllers/users-secure-controller.js";
import {check} from 'express-validator';

// router.post('/registration', [
//     check('username',"Ім'я користувача не може бути порожнім").notEmpty(),
//     check('password',"Пароль повинен бути коротшим за 15 і більше 3 символів").isLength({
//         max: 15,
//         min: 3
//     })
// ], auth.registrations);

router.post('/login', auth.login);

export default router;