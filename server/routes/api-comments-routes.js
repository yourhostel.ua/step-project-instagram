import express from 'express';
const router = express.Router()
import {
    getJsonAll,
    getJson,
    deleteObj,
    addObj,
    editObj,
} from '../controllers/api-comments-controller.js';

//повертаємо всі об'єкти в JSON-не
router.get('/api/comments', getJsonAll)
//повертаємо один об'єкт по id в JSON-не
router.get('/api/comments/:id', getJson)
//видаляємо об'єкт
router.delete('/api/comments/delete/:id', deleteObj)
//постимо на сервер
router.post('/api/comments/add', addObj);
//обробка put-запитів редагування
router.put('/api/comments/edit/:id', editObj);

export default router;