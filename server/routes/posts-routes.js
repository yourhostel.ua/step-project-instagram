import express from 'express';
const router = express.Router()
import {
    deletePage,
    deleteObj,
    addPage,
    addObj,
    editPage,
    editObj,
} from '../controllers/posts-controller.js';

//тестова сторінка видалення
router.get('/posts/delete', deletePage)
//видаляємо об'єкт
router.delete('/posts/delete/:id', deleteObj)
//тестова сторінка додавання
router.get('/posts/add', addPage)
//постимо на сервер
router.post('/posts/add', addObj);
//тестова сторінка редагування
router.get('/posts/edit/:id', editPage);
//обробка put-запитів редагування
router.put('/posts/edit/:id', editObj);

export default router;