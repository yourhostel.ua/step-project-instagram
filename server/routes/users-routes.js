import express from 'express';
const router = express.Router()
import {
    deletePage,
    deleteObj,
    addPage,
    addObj,
    editPage,
    editObj,
} from '../controllers/users-controller.js';

//тестова сторінка видалення
router.get('/users/delete', deletePage)
//видаляємо об'єкт
router.delete('/users/delete/:id', deleteObj)
//тестова сторінка додавання
router.get('/users/add', addPage)
//постимо на сервер
router.post('/users/add', addObj);
//тестова сторінка редагування
router.get('/users/edit/:id', editPage);
//обробка put-запитів редагування
router.put('/users/edit/:id', editObj);

export default router;
