import express from 'express';
const router = express.Router()
import {
    getJsonAll,
    getJson,
    deleteObj,
    addObj,
    editObj,
} from '../controllers/api-posts-controller.js';

//повертаємо всі об'єкти в JSON-не
router.get('/api/posts', getJsonAll)
//повертаємо один об'єкт по id в JSON-не
router.get('/api/post/:id', getJson)
//видаляємо об'єкт
router.delete('/api/posts/delete/:id', deleteObj)
//постимо на сервер
router.post('/api/posts/add', addObj);
//обробка put-запитів редагування
router.put('/api/posts/edit/:id', editObj);

export default router;