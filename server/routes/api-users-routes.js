import express from 'express';
const router = express.Router()
import {
    getJsonAll,
    getJson,
    deleteObj,
    addObj,
    editObj,
} from '../controllers/api-users-controller.js';
import authMiddleware from "../utils/authMiddleware.js";

//повертаємо всі об'єкти в JSON-не
router.get('/api/users', getJsonAll)
//повертаємо один об'єкт по id в JSON-не
router.get('/api/user/:id', getJson)
//видаляємо об'єкт
router.delete('/api/users/delete/:id', deleteObj)
//постимо на сервер
router.post('/api/users/add', addObj);
//обробка put-запитів редагування
router.put('/api/users/edit/:id', editObj);

export default router;
