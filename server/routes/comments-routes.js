import express from 'express';
const router = express.Router()
import {
    deletePage,
    deleteObj,
    addPage,
    addObj,
    editPage,
    editObj,
} from '../controllers/comments-controller.js';

//тестова сторінка видалення
router.get('/comments/delete', deletePage)
//видаляємо об'єкт
router.delete('/comments/delete/:id', deleteObj)
//тестова сторінка додавання
router.get('/comments/add', addPage)
//постимо на сервер
router.post('/comments/add', addObj);
//тестова сторінка редагування
router.get('/comments/edit/:id', editPage);
//обробка put-запитів редагування
router.put('/comments/edit/:id', editObj);

export default router;