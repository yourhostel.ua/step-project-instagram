import Users from '../models/users.js';
import createPath from '../helpers/create-path.js';
const title = "users";
const pattern = ['nickName', 'fullName', 'photo', 'friends'];

export const deletePage =(req,res)=>{
    Users
        .find()
        .then(items => res.render(createPath('delete'), {items, title}))
        .catch(error => res.send(error))
}

export const deleteObj =(req,res)=>{
    Users
        .findByIdAndDelete(req.params.id)
        .then(_ => res.sendStatus(200))
        .catch(error => res.send(error))
}

export const addPage =(req,res)=>{
    res.render(createPath('add'), {title, pattern})
}

export const addObj =(req,res)=>{
    const {nickName, fullName, photo, friends} = req.body
    const users = new Users({
        nickName,
        fullName,
        photo,
        friends: JSON.parse(friends),
        time: new Date().toLocaleString(),
    })
    users
        .save()
        .then((reg) => {
            const res_serv = reg._doc;
            res.render(createPath('response'), {res_serv, title});
        })
        .catch(error => res.send(error))
}

export const editPage =(req,res)=>{
    Users
        .findById(req.params.id)
        .then((reg) => {
            const item = reg._doc;

            res.render(createPath('edit'), {item, title});
        })
        .catch(error => res.send(error))
}

export const editObj =(req,res)=>{
    let {nickName, fullName, photo, friends} = req.body, {id} = req.params;
    friends = JSON.parse(friends);
    Users
        .findByIdAndUpdate(id, {nickName, fullName, photo, friends})
        .then(() => res.redirect(`/${title}/edit/${id}`))
        .catch(error => res.status(500).send(error))
}

