import User from '../models/User.js';
import Role from '../models/Role.js';
import bcrypt from 'bcryptjs';
import {validationResult} from 'express-validator';
import jwt from 'jsonwebtoken';
import secure from "../secure/config.js"
import createPath from "../helpers/create-path.js";

const generateAccessToken = (id, roles) => {
    const payload = {
        id,
        roles
    }
    return jwt.sign(payload, secure.secret, {expiresIn: "30m"})//час життя токена
}

class authController {
    // async registrations(req, res) {
    //     try {
    //         const errors = validationResult(req)
    //         if (!errors.isEmpty()) {
    //
    //             return res.status(400).json({massage: "Помилка під час реєстрації", errors})
    //         }
    //
    //         const {username, password} = req.body;
    //         const candidate = await User.findOne({username})
    //
    //         if (candidate) {
    //             return res.status(400).json({massage: "Користувач із таким ім'ям вже існує"})
    //         }
    //
    //         const hashPassword = bcrypt.hashSync(password, 4);
    //         const userRole = await Role.findOne({value: "USER"})
    //         const user = new User({username, password: hashPassword, roles: [userRole.value]})
    //
    //         await user.save()
    //
    //         return res.json({massage: "Користувач успішно зареєстрований!"})
    //
    //     } catch (err) {
    //         console.log(err)
    //         res.status(400).json({massage: "Registration error!"})
    //     }
    // }

    async login(req, res) {
        try {
            const {username, password} = req.body; //читаємо POST запит
            const user = await User.findOne({username})//запитуємо у базі

            if (!user) { //якщо користувача немає в базі
                return res.status(400)
                    .render(createPath('answer'), {massage: `Користувач ${username} не знайдений`})
            }

            const validPassword = bcrypt.compareSync(password, user.password) //порівнюємо введений пароль та хеш пароля з бази

            if (!validPassword) {
                return res
                    .status(400)
                    .render(createPath('answer'), {massage: "Введено невірний пароль"})
            }
            const token = generateAccessToken(user._id, user.roles)//якщо все ок генеруємо і відправляємо токен
            return res
                .render(createPath('answer'), {
                    title: "Tокен отримано! діє 30 хвилин",
                    massage: `${token}`,
                    token: JSON.stringify(token)
                })
        } catch (err) {
            console.log(err)
            res
                .status(400)
                .render(createPath('answer'), {massage: "Login error!"})
        }
    }
}

export default new authController()

// const userRole = new Role() //
// const adminRole = new Role({value:'ADMIN'})
// await userRole.save()
// await adminRole.save()

