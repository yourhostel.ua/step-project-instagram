import Users from '../models/users.js';

export const getJsonAll =(req,res)=>{
    Users
        .find()
        .then(item => res.status(200).json(item))
        .catch(error => res.status(500).send(error))
}

export const getJson =(req,res)=>{
    Users
        .findById(req.params.id)
        .then(item => res.status(200).json(item))
        .catch(error => res.status(500).send(error))
}

export const deleteObj =(req,res)=>{
    Users
        .findByIdAndDelete(req.params.id)
        .then(_ => res.sendStatus(200))
        .catch(error => res.status(500).send(error))
}

export const addObj =(req,res)=>{
    const {nickName, fullName, photo, friends} = req.body
    const users = new Users({
          nickName,
          fullName,
          photo,
          friends: JSON.parse(friends),
          time: new Date().toLocaleString(),
    })
    users
        .save()
        .then(user=> res.status(200).json(user))
        .catch(error => res.status(500).send(error))
}

export const editObj =(req,res)=>{
    let {nickName, fullName, photo, friends} = req.body, {id} = req.params;
    friends = JSON.parse(friends);
    Users
        .findByIdAndUpdate(id, {nickName, fullName, photo, friends}, {new:true})
        .then(user => res.status(200).json(user))
        .catch(error => res.status(500).send(error))
}