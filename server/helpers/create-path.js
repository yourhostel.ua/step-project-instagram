import path, {dirname} from "path";
import {fileURLToPath} from "url";
const __filename = fileURLToPath(import.meta.url);
export const __dirname = dirname(__filename);
const createPath = page => path.resolve(__dirname, '../views', `${page}.ejs`);

export default createPath
