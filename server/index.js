import chalk from 'chalk'; //крейда для повідомлень
const errMsg = chalk.bgWhite.red
const successMsg = chalk.bgGreen.white

import * as dotenv from 'dotenv'//шифрування доступу
dotenv.config()

import createServer from './utils/config.js';//мидлвары
import mongoose from 'mongoose';//база данных

const app = createServer();

mongoose //запускаємо базу
    .set('strictQuery', false)
    .connect(process.env.MONGO_URL, {useNewUrlParser: true, useUnifiedTopology: true})
    .then(() => console.log(successMsg('Connected to mongodb+srv')))
    .catch(error => console.log(errMsg(error)))

app //запускаємо сервер
    .set('view engine', 'ejs')
    .listen(process.env.PORT, (error) => {
        if (error) {
            console.log(errMsg(error))
        } else {
            console.log(successMsg(`Server running on http://localhost:${process.env.PORT}`));
        }
    });
