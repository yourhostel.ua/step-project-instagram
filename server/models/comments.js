import mongoose from 'mongoose';

const commentsSchema = new mongoose.Schema({
    userId: {
        type: String,
        required: true,
    },
    postId:{
        type: String,
        required: true,
    },
    text: {
        type: String,
        required: true,
    },
    time: {
        type: String,
    },
},{timestamps: true});

const Comments = mongoose.model("comments", commentsSchema);

export default Comments