import mongoose from 'mongoose';

const usersSchema = new mongoose.Schema({
    nickName: {
        type: String,
        required: true,
    },
    fullName: {
        type: String,
        required: true,
    },
    photo: {
        type: String,
        required: true,
    },
    friends: {
        type: Array,
        required: true,
    },
    time: {
        type: String,
    },
},{timestamps: true});

const Users = mongoose.model("users", usersSchema);

export default Users
