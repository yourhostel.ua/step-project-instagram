import {createSlice} from '@reduxjs/toolkit';
import defaultInitialState from './defaultState';
import {setTest, clearTest} from './reducersTest';
import {loadUsers, loadUser, loadPosts, loadPost, editPost, loadComments, sendComment} from './asyncThunks';
import myBuilder from './builders';

const slice = createSlice({
    name: 'my',
    initialState: defaultInitialState,
    reducers: {
        setIsOpen(state, action) {
            state.isOpen = action.payload;
            if (action.payload === false){
                state.post ={
                    arr: {},
                    status: '',
                    err: '',
                }
            }
        },
        setFriends(state, action) {
            state.friends = action.payload;
        },
        setLikes(state, action) {
            state.like = action.payload;
        },
        setIsFriend(state, action) {
            state.isFriend = action.payload;
        },
        setExtraReducersTest(state, action){
            setTest(state, action);
        },
        clearExtraReducersTest(state){
            clearTest(state);
        }
    },
    extraReducers: builder => {
        myBuilder(builder, loadUsers, 'users');
        myBuilder(builder, loadUser, 'user');
        myBuilder(builder, loadPosts, 'posts');
        myBuilder(builder, loadPost, 'post');
        myBuilder(builder, editPost, 'post');
        myBuilder(builder, loadComments, 'comments');
        myBuilder(builder, sendComment, 'comment');
    }
})

export const {
    setIsOpen,
    setUserId,
    setFriends,
    setPostId,
    setActiveAccount,
    setIsFriend,
    setLikes,
    setExtraReducersTest,
    clearExtraReducersTest
} = slice.actions;

export default slice.reducer;
