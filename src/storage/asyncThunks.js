import {createAsyncThunk} from "@reduxjs/toolkit";

const loadUsers = createAsyncThunk(
    'my/users',
    async (_, {rejectWithValue}) => {
        try {
            const res = await fetch('/api/users')

            if(!res.ok){
                throw new Error(`Помилка`)
            }
            return await res.json();
        } catch (err) {
            return rejectWithValue(err.message);
        }
    }
);

const loadUser = createAsyncThunk(
    'my/user',
    async (id, {rejectWithValue}) => {
        try {
            const res = await fetch(`/api/user/${id}`)// /source - test
            if(!res.ok){
                throw new Error(`Помилка`)
            }
            return await res.json();
        } catch (err) {
            return rejectWithValue(err.message);
        }
    }
);

const loadPosts = createAsyncThunk(
    'my/posts',
    async (_, {rejectWithValue}) => {
        try {
            const res = await fetch('/api/posts');
            if(!res.ok){
                throw new Error(`Помилка`)
            }
            const result = await res.json();
            return result.sort((a, b)=> Date.parse(b.time) - Date.parse(a.time))
        } catch (err) {
            return rejectWithValue(err.message);
        }
    }
);

const loadPost = createAsyncThunk(
    'my/post',
    async (id, {rejectWithValue}) => {
        try {
            const res = await fetch(`/api/post/${id}`)
            return await res.json();
        } catch (err) {
            return rejectWithValue(err.message);
        }
    }
);

const editPost = createAsyncThunk(
    'my/editPost',
    async (obj, {rejectWithValue}) => {
        try {
            const myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/x-www-form-urlencoded");

            const urlencoded = new URLSearchParams();
            for (let key in obj) {
                if (!key.match(/^id|\+$/)) {
                    urlencoded.append(`${key}`, `${obj[key]}`);
                }
            }

            const requestOptions = {
                method: 'PUT',
                headers: myHeaders,
                body: urlencoded,
                redirect: 'follow'
            };

            const res = await fetch(`/api/posts/edit/${obj.id}`, requestOptions)

            if(!res.ok){
                throw new Error(`Перевірте заповнення полів`)
            }

            return await res.json();

        } catch (err) {
            return rejectWithValue(err.message);
        }
    }
);

const loadComments = createAsyncThunk(
    'my/comments',
    async (_, {rejectWithValue}) => {
        try {
            const res = await fetch('/api/comments');
            if(!res.ok){
                throw new Error(`Помилка`)
            }
            return await res.json();
        } catch (err) {
            return rejectWithValue(err.message);
        }
    }
);

const sendComment = createAsyncThunk(
    'my/comment',
    async (obj, {rejectWithValue}) => {
        try {
            const myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/x-www-form-urlencoded");
            const urlencoded = new URLSearchParams();
            for (let key in obj) {
                    urlencoded.append(`${key}`, `${obj[key]}`);
            }

            const requestOptions = {
                method: 'POST',
                headers: myHeaders,
                body: urlencoded,
                redirect: 'follow'
            };
            const res = await fetch(`/api/comments/add/`, requestOptions)

            if(!res.ok){
                throw new Error(`Перевірте заповнення полів`)
            }

            return await res.json();

        } catch (err) {
            return rejectWithValue(err.message);
        }
    }
);

const editUser = createAsyncThunk(
    'my/editUser',
    async (obj, {rejectWithValue}) => {
        try {
            const myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/x-www-form-urlencoded");
            const urlencoded = new URLSearchParams();
            for (let key in obj) {
                if (!key.match(/^id|\+$/)) {
                    urlencoded.append(`${key}`, `${obj[key]}`);
                }
            }

            const requestOptions = {
                method: 'PUT',
                headers: myHeaders,
                body: urlencoded,
                redirect: 'follow'
            };

            const res = await fetch(`/api/users/edit/${obj.id}`, requestOptions)

            if(!res.ok){
                throw new Error(`Перевірте заповнення полів`)
            }

            await res.json();

        } catch (err) {
            return rejectWithValue(err.message);
        }
    }
);

export {loadUsers, loadUser, editUser, loadPosts, loadPost, editPost, loadComments, sendComment}