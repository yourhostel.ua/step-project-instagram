const defaultInitialState = {
    isOpen: false,
    users: {
        arr: [],
        status: '',
        err: '',
    },
    user: {
        arr: {},
        status: '',
        err: '',
    },
    friends: [],
    posts: {
        arr: [],
        status: '',
        err: '',
    },
    comments:{
        arr: [],
        status: '',
        err: '',
    },
    comment:{
        arr: '',
        status: '',
        err: '',
    },
    post: {
        arr: {},
        status: '',
        err: '',
    },
    activeAccount: {
        id: '63e7af1995845939c286cb00',
        nickName: 'forest_inspire',
        photo: '/image/user/280231713.jpg',
        fullName: 'Forest Inspire',
    },
    like: [],
    extraReducersTest: {
        fnk: 'editPost',
        reducerField: 'post',
    },
    isFriend:[],
}

export default defaultInitialState