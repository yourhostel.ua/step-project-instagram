const setTest = (state, action) => {
    state.extraReducersTest.fnk = action.payload;
    switch (action.payload){
        case 'loadUsers': state.extraReducersTest.reducerField = 'users';
            break
        case 'loadUser': state.extraReducersTest.reducerField = 'user';
            break
        case 'loadPosts': state.extraReducersTest.reducerField = 'posts';
            break
        case 'loadPost': state.extraReducersTest.reducerField = 'post';
            break
        case 'loadComments': state.extraReducersTest.reducerField = 'comments';
            break
        case 'sendComment': state.extraReducersTest.reducerField = 'comment';
            break
        default : state.extraReducersTest.reducerField = 'post';
    }
}

const clearTest = (state) => {
    state.users.status = "";
    state.users.arr = [];
    state.users.err = "";
    state.user.status = "";
    state.user.arr = {};
    state.user.err = "";
    state.posts.status = "";
    state.posts.arr = [];
    state.posts.err = "";
    state.post.status = "";
    state.post.arr = {};
    state.post.err = "";
    state.comments.status = "";
    state.comments.arr = [];
    state.comments.err = "";
    state.comment.status = "";
    state.comment.arr = {};
    state.comment.err = "";
}

export {setTest, clearTest};