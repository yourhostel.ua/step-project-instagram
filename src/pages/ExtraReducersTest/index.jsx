import './style.scss'
import Output from "./Output";
import Unloading from "./Unloading";
import field from "./options";
import {useDispatch, useSelector} from "react-redux";
import {
    loadUsers,
    loadUser,
    loadPosts,
    loadPost,
    editPost,
    loadComments,
    sendComment
} from "../../storage/asyncThunks";
import {setExtraReducersTest, clearExtraReducersTest} from "../../storage/slice"

const ExtraReducersTest = () => {
    const dispatch = useDispatch(),
          my = useSelector(state => state.my),
          test = useSelector(state => state.my.extraReducersTest.fnk),
          reducerField = useSelector(state => state.my.extraReducersTest.reducerField);
      let myField = my[reducerField];

    const handleSubmit = event => {
        event.preventDefault()
        switch (test){
            case 'loadUsers': dispatch(loadUsers());
                break
            case 'loadUser': dispatch(loadUser(Object.fromEntries(new FormData(event.target)).id));
                break
            case 'loadPosts': dispatch(loadPosts());
                break
            case 'loadPost': dispatch(loadPost(Object.fromEntries(new FormData(event.target)).id));
                break
            case 'loadComments': dispatch(loadComments());
                break
            case 'sendComment': dispatch(sendComment(Object.fromEntries(new FormData(event.target))));
                break
            default : dispatch(editPost(Object.fromEntries(new FormData(event.target))));
        }
    }
    const handleChange = event => {
       dispatch(setExtraReducersTest(event.target.value))
       dispatch(clearExtraReducersTest(event.target.value))
    }

    return (
        <div className="wr">
            <h1 className="wr_title">Extra Reducers Test</h1>
            <div className="wr_title title--size-a title--color-a">function
                <select onChange={handleChange}
                        className="wr_title title--size-a title--color-b" name="selectedFruit" defaultValue="editPost">
                    <option value="loadUsers">loadUsers</option>
                    <option value="loadUser">loadUser</option>
                    <option value="loadPosts">loadPosts</option>
                    <option value="loadPost">loadPost</option>
                    <option value="editPost">editPost</option>
                    <option value="loadComments">loadComments</option>
                    <option value="sendComment">sendComment</option>
                </select>
            </div>
            <form onSubmit={handleSubmit} className="wr_form">
                {myField.status && <Output>{myField.status}</Output>}
                {myField.err && <Output>{myField.err}</Output>}
                {
                    field[test].map(_=> (
                        <label key={Math.random()}>{_?.field}:
                            <p className="wr_form_example"><span>example: </span><span>{_?.example}</span></p>
                            <input name={_?.field}/>
                        </label>))
                }
                <div className="btn">
                    <input className="btn_btn" type="submit" value="Надіслати"/>
                </div>
                <div>
                    {
                        Array.isArray(myField.arr) ? myField.arr.map(_ => (
                             <Unloading key={Math.random()} obj={_}/>
                        )) : <Unloading obj={myField.arr}/>
                    }
                </div>
            </form>
        </div>
    );
}

export default ExtraReducersTest