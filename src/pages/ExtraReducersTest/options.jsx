import {loadComments} from "../../storage/slice";

const field = {
    loadUsers: [],
    loadUser: [{
        field: "id",
        example: "63e97275b7ad4b312a28e2f0",
    },],
    loadPost: [{
        field: "id",
        example: "63e97275b7ad4b312a28e2f0",
    }],
    loadPosts: [],
    editPost: [{
        field: "id",
        example: "63e97275b7ad4b312a28e2f0",
    }, {
        field: "userId",
        example: "63e97275b7ad4b312a28e2f0",
    }, {
        field: "text",
        example: "Facere, quas!",
    }, {
        field: "photo",
        example: "/img/foto.jpg",
    }, {
        field: "likes",
        example: "[1, \"test2\", 32, \"45\"]",
    },],
    loadComments: [],
    sendComment: [{
        field: "userId",
        example: "63e97275b7ad4b312a28e2f0",
    }, {
        field: "postId",
        example: "63e97275b7ad4b312a28e2f0",
    }, {
        field: "text",
        example: "Facere, quas!",
    },],
}

export default field