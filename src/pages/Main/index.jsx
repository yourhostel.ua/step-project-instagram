import Post from "../../components/Post";
import Avatar from "../../components/Avatar";
import './style.scss'
import {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {loadUser, loadUsers, loadPosts, loadComments} from "../../storage/asyncThunks"
import {setFriends, setLikes, setIsFriend, setIsOpen} from "../../storage/slice";
import {useParams} from "react-router-dom";
import InfiniteScroll from 'react-infinite-scroll-component'
import Loader from "../../components/Loader";

export default function Main ({likePhoto, addFriend}){
    const dispatch = useDispatch()
    const {users, friends, posts, account, like, isFriend,comments,usersStatus,postsStatus, error, user} = useSelector(state => ({
        users: state.my.users.arr,
        user: state.my.user.arr,
        usersStatus: state.my.users.status,
        postsStatus: state.my.posts.status,
        error: state.my.users.err,
        friends: state.my.friends,
        posts: state.my.posts.arr,
        account: state.my.activeAccount,
        like: state.my.like,
        isFriend:state.my.isFriend,
        comments: state.my.comments.arr
    }))
    useEffect(() => {dispatch(loadUsers())}, [dispatch]);
    useEffect(() => {dispatch(loadPosts()) },[dispatch]);
    useEffect(() => {dispatch(loadComments()) },[dispatch]);
    useEffect(() => {dispatch(setIsOpen(false))}, [true]);
    useEffect(()=>{
        dispatch(setLikes(posts.filter(el=>el.likes.includes(account.id) ).map(e => e._id)))
    }, [posts])
    useEffect(()=>{
        dispatch(setFriends(users.filter(el=>el._id === account.id)[0]?.friends || []))
    }, [users])
    useEffect(()=>{
        dispatch(setIsFriend(users.filter(el=>el._id === account.id)[0]?.friends || []))
    }, [users])
    const param = useParams()
    useEffect(()=>{
        param.id && dispatch(loadUser(param.id))
    }, [param.id])
    const [items, setItems] = useState([])
    const [noMore, setNoMore] = useState(true)
    useEffect(()=>{
        if (items.length>0){
            setItems(posts.filter((e, i)=> i<items.length))
        }else {
            setItems(posts.filter((e, i)=> i<3))
        }
    }, [posts])

    const fetchData = ()=>{
        if (items.length === posts.length){
            setNoMore(false)
        } else{
            let more = posts.filter((e, i)=> {
                return i >= items.length && i < items.length+3
            })
            setItems([...items, ...more])
        }
    }

    if (error){
        return <p>Something get wrong</p>
    } else {
        if (postsStatus === 'pending' && usersStatus === 'pending'){
            return <Loader/>
        } else{
            return (
                <main className='main'>
                    <section className='main__post-section'>
                        {<InfiniteScroll
                            dataLength={items.length}
                            next={fetchData}
                            hasMore={noMore}
                            loader={<Loader/>}
                            showLoader={true}
                        >
                            {items.map(({photo, _id, userId, likes, text}) => {
                                let comment = comments.filter(e => e.postId === _id)
                                return (
                                    <Post type='post' active={true}
                                          postImg={photo}
                                          nickName={users.find(el => el._id === userId).nickName}
                                          photo={users.find(el => el._id === userId).photo}
                                          key={_id}
                                          userId={userId}
                                          likePhoto={likePhoto}
                                          postId={_id}
                                          isLike={!!like.includes(_id)}
                                          likes={likes}
                                          text={text}
                                          comments={comment}
                                          user={users}
                                          commentsLength={comment.length}
                                          status={postsStatus}
                                    />
                                )
                            })}
                        </InfiniteScroll>}
                    </section>
                    <section className='main__profiles-section'>
                        <div className="main__profiles-section__avatar-sec">
                            <Avatar className="main__profiles-section__avatar-sec__avatar"
                                    nickName={account.nickName}
                                    photo={account.photo}
                                    id={account.id}
                                    fullName={account.fullName}
                                    userId={user._id}
                            />
                        </div>
                        <div className='main__profiles-section__subscribers'>
                            <div className='main__profiles-section__subscribers__header'>
                                <p>Розповіді</p>
                                <p>Переглянути все</p>
                            </div>
                            <ul  className='main__profiles-section__subscribers__list'>
                                {usersStatus==='pending' ? <Loader/> : users.map(({_id, nickName, photo, fullName})=>{
                                    return (friends.includes(_id) && _id !== account.id && <li>
                                        <Avatar className="avatar-container__info__icon"
                                                action={false}
                                                nickName={nickName}
                                                photo={photo}
                                                key={_id}
                                                id={_id}
                                                userId={user._id}
                                                fullName={fullName}/>
                                    </li>)
                                })}
                            </ul>
                        </div>
                        <div className='main__profiles-section__recomn'>
                            <div className='main__profiles-section__recomn__header'>
                                <p>Рекомендації для вас</p>
                                <p>Переглянути все</p>
                            </div>
                            <ul  className='main__profiles-section__recomn__list'>
                                {usersStatus==='pending' ? <Loader/> : users.map(({_id, nickName, photo, fullName})=>{
                                    return (!friends.includes(_id) && _id !== account.id && <li>
                                        <Avatar
                                            className="avatar-container__info__icon avatar-container__info__icon--recomn"
                                            action={isFriend.includes(_id) ? 'Відписатися' : 'Підписатися'}
                                            nickName={nickName}
                                            photo={photo}
                                            key={_id}
                                            id={_id}
                                            userId={user._id}
                                            clickHandler={addFriend}
                                            fullName={fullName}
                                        />
                                    </li>)
                                })}
                            </ul>
                        </div>
                        <footer className='main__profiles-section__footer'>
                            <p className='main__profiles-section__footer__links'>
                                <a href="/">Информация &middot;</a>
                                <a href="/">Помощь &middot;</a>
                                <a href="/">Пресса &middot;</a>
                                <a href="/">API &middot;</a>
                                <a href="/">Ваканси &middot;</a>
                                <br/>
                                <a href="/">Конфиденциальность &middot;</a>
                                <a href="/">Условия &middot;</a>
                                <a href="/">Места &middot;</a>
                                <a href="/">Язык</a>
                            </p>
                            <p>&copy; 2023 INSTAGRAM FROM META</p>
                        </footer>
                    </section>
                </main>
            )
        }
    }


}
