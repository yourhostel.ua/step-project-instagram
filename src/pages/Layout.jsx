import {Outlet} from "react-router-dom";
import Aside from "../components/Aside";

export default function Layout () {

    return (
        <div className='container'>
        <Aside/>
        <Outlet/>
        </div>
    )
}