import IconMain from "./icons/IconMain"
import IconSearch from "./icons/IconSearch"
import IconInteresting from "./icons/IconInteresting"
import IconReels from "./icons/IconReels"
import IconMessage from "./icons/IconMessage"
import IconNotification from "./icons/IconNotification";
import IconCreate from "./icons/IconCreate";
import IconProfile from "./icons/IconProfile";

const field = [{
    item: "Головна",
    icon: <IconMain/>,
    link: "/",
    key:  "1",
}, {
    item: "Extra reducers test",//Пошуковий запит
    icon: <IconSearch/>,
    link: "/extra-reducers-test",
    key:  "2",
}, {
    item: "Цікаве",
    icon: <IconInteresting/>,
    link: "/",
    key: "3",
}, {
    item: "Reels",
    icon: <IconReels/>,
    link: "/",
    key: "4",
}, {
    item: "Повідомлення",
    icon: <IconMessage/>,
    link: "/",
    key: "5",
}, {
    item: "Сповіщення",
    icon: <IconNotification/>,
    link: "/",
    key: "6",
}, {
    item: "Створити",
    icon: <IconCreate/>,
    link: "/",
    key: "7",
}, {
    item: "Профіль",
    icon: <IconProfile/>,
    link: "profile",
    key: "8",
},]

export default field