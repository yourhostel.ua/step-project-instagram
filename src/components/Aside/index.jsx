import {Link, useParams} from "react-router-dom";
import {useEffect} from "react";
import {setActiveAccount} from "../../storage/slice";
import {useDispatch, useSelector} from "react-redux";

import LogoFull from "./icons/LogoFull"
import LogoSmall from "./icons/LogoSmall"
import IconMore from "./icons/IconMore";
import LiItem from "./LiItem"

import field from "./options"
import {loadUser} from "../../storage/asyncThunks";

export default function Aside(){
    const dispatch = useDispatch()
    const {account} = useSelector(state => ({
        account: state.my.activeAccount
    }))
    function clickHandler(){
        // dispatch(loadUser(account.id))
    }
    return (
        <aside  className='aside'>
            <div  className='aside__header'><LogoFull/><LogoSmall/></div>
            <nav className='aside__main'>
                <ul  className='aside__main__list'>
                    {
                        field.map(_=> {
                            if (_.link !== "profile") {
                                return <LiItem
                                    key={_.key}
                                    icon={_.icon}
                                    item={_.item}
                                    to={_.link}/>
                            } else {
                                return <LiItem
                                    key={_.key}
                                    icon={_.icon}
                                    item={_.item}
                                    to={"/"+_.link+"/"+account.id}
                                    onClick={clickHandler}
                                />
                            }
                        })
                    }
                </ul>
            </nav>
            <div className='aside__footer'>
                <Link to='/'  className='aside__footer__link'>
                    <IconMore/>
                    <p>Ще</p>
                </Link>
            </div>
        </aside>
    )
}

