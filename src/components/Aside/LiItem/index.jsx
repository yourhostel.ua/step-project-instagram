import {Link} from "react-router-dom";

const LiItem =({onClick, to, icon, item})=> {
    return (
        <li  className='aside__main__list__item' onClick={onClick}>
            <Link to={to}>
                {icon}
                <p>{item}</p>
            </Link>
        </li>
    )
}
export default LiItem