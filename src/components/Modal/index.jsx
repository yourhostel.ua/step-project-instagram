import './style.scss'
import Post from "../Post";
import Button from "../Button";
import CloseIcon from "../Icons/CloseIcon";

export default function Modal ({hideModal, postImg, nickName,
                                   likes, userPhoto,status, id, userId,
                                   userClick,likePhoto, isLike, text,comments,user}) {

    return (
        <div className='modal' onClick={hideModal}>
            <div className='modal__content'>
                <Post type='modal' clickHandler={e => e.stopPropagation()}
                       postImg={postImg}
                       nickName={nickName}
                       photo={userPhoto}
                       key={id}
                       userId={userId}
                       likePhoto={likePhoto}
                       isLike={isLike}
                       text={text}
                       postId={id}
                       likes={likes}
                       user={user}
                       comments={comments}
                       status={status}
                      userClick={userClick}
                />
                <Button btnText={<CloseIcon/>}  className='modal__content__close-btn' onClick={hideModal}/>
            </div>
        </div>
    )
}