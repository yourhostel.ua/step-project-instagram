const CommentsIcon = ({isPhotoPost}) => {
    return (
        <svg aria-label="Комментировать" className="_ab6-" color={isPhotoPost ? "#ffffff": "#262626"} fill="#262626" height="24" role="img"
             viewBox="0 0 24 24" width="24">
            <path d="M20.656 17.008a9.993 9.993 0 1 0-3.59 3.615L22 22Z" fill="none" stroke="currentColor"
                  strokeLinejoin="round" strokeWidth="2"></path>
        </svg>
    )
}

export default CommentsIcon