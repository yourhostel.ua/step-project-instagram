import {Link, useParams} from "react-router-dom";
import './index.scss'
import Button from "../Button";
import {logDOM} from "@testing-library/react";
export default function Avatar ({status, className, action, comments, nickName, photo, userClick, id, userId, clickHandler, fullName}){
    const param = useParams()
    return (
        <div className='avatar-container'>
            <div className='avatar-container__info'>
            <Link to={`/profile/${id}`} onClick={userClick}>
                <img src={photo} alt="userPhoto"  className={className}/></Link>
                <div className='avatar-container__info__nickname'>
                    <p><Link to={`/profile/${id}`}  onClick={userClick}>{nickName} </Link>
                        <span className='avatar-container__info__nickname__comment'> {comments}</span></p>
                </div>
            </div>
            <Button className='avatar-container__action' btnText={action}
                    clickHandler={(e)=>clickHandler(e, {id}, {nickName}, {fullName}, {photo})}
            ></Button>
        </div>
    )
}