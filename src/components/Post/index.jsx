import './styles.scss'
import Avatar from "../Avatar";
import Button from "../Button";
import DotsIcon from "../Icons/Dots";
import HeartIcon from "../Icons/Heart";
import CommentsIcon from "../Icons/Comments";
import ShareIcon from "../Icons/Share";
import SaveIcon from "../Icons/Save";
import Form from "../Form";
import {useState, useRef} from "react";
import Loader from "../Loader";
import {useSelector, useDispatch,} from "react-redux";
import {loadComments, sendComment} from "../../storage/asyncThunks"

export default function Post ({type, clickHandler, nickName, photo, userId, userClick, postImg, likePhoto, postId, isLike, likes, text, comments, user,commentsLength}){
    const form = useRef(null)
    const [showComments, setShowComments] = useState(false)
    const dispatch = useDispatch()
    function showMore(){setShowComments(true)}
    const {commentStatus, activeUser} = useSelector(state => ({
        commentStatus: state.my.comments.status,
        activeUser: state.my.activeAccount.id
    }))
    const handleSubmit = async event => {
        event.preventDefault()
        const obj = {
            userId: activeUser,
            postId: postId,
            text: Object.fromEntries(new FormData(event.target)).text,
        }
        await dispatch(sendComment(obj))
        dispatch(loadComments())
        form.current.value = ""
    }

    return (
        <>
            <article className={type+'-container'} onClick={clickHandler}>
               <div className={type+'-container__header'}>
                    <Avatar className="avatar-container__info__icon"
                            nickName={nickName}
                            photo={photo}
                            key={userId}
                            id={userId}
                            userId={userId}
                            userClick={userClick}/>
                    <Button btnText={<DotsIcon/>}/>
                </div>
                <div className={type+'-container__main'}
                     onDoubleClick={(e)=>likePhoto(e, {postId}, {likes}, {userId}, {text}, {postImg})}>
                    <img src={postImg} alt="postPhoto"/>
                </div>
                <div className={type+'-container__footer'}>
                    <div className={type+'-container__footer__icons-container'}>
                        <div className={type+'-container__footer__icons-container__icons'}>
                            <HeartIcon
                                isLike={isLike}
                                clickHandler={(e)=>likePhoto(e, {postId}, {likes}, {userId}, {text}, {postImg})}
                            />
                            <CommentsIcon/>
                            <ShareIcon/>
                        </div>
                        <SaveIcon/>
                    </div>
                    <div className={type+'-container__footer__comments'}>
                            <ul  className={type+'-container__footer__comments__list'}>
                                {commentStatus === 'pending' ? <Loader/> : (type === 'post' ? (comments.map((el, i)=>{
                                    if (i === 0) {
                                        return (<li key={el._id}>
                                            <Avatar comments={el.text}
                                                    className={type + '-container__footer__comments__list__item'}
                                                    nickName={user.find(e => e._id === el.userId).nickName}
                                                    photo={(user.find(e => e._id === el.userId)).photo}
                                                    userId={el.userId}
                                                    userClick={userClick}
                                                    id={el.userId}
                                            /></li>)
                                    } else {
                                        return (showComments && <li key={el._id}>
                                            <Avatar comments={el.text}
                                                    className={type + '-container__footer__comments__list__item'}
                                                    nickName={user.find(e => e._id === el.userId).nickName}
                                                    photo={(user.find(e => e._id === el.userId)).photo}
                                                    userId={el.userId}
                                                    userClick={userClick}
                                                    id={el.userId}
                                            /></li>)
                                    }
                                })) : (comments.map((el)=>{
                                        return (<li key={el._id}>
                                            <Avatar comments={el.text}
                                                    className={type + '-container__footer__comments__list__item'}
                                                    nickName={user.find(e => e._id === el.userId).nickName}
                                                    photo={(user.find(e => e._id === el.userId)).photo}
                                                    userId={el.userId}
                                                    userClick={userClick}
                                                    id={el.userId}
                                            /></li>)
                                })))}
                            </ul>
                    </div>
                    {commentsLength>1 && !showComments && <Button className={type + '-container__footer__comments__showAll'}
                             btnText={`Переглянути всі коментарі (${commentsLength-1})`}
                             clickHandler={showMore}
                    />}
                    <Form reference={form} onSubmit={handleSubmit} className={type+'-container__footer__comments__input'}/>
                </div>
            </article>
        </>

    )
}